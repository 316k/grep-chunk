#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <re2/re2.h>
#include "args.h"

using namespace std;

int main(int argc, const char** argv) {

	bool alternate = false, ignore_case = false;
	string until{""}, after{""};

	ARGBEGIN
		LSARG_CASE('a', "after")
		after = ARGS;
	LSARG_CASE('u', "until")
		until = ARGS;
	LSARG_CASE('x', "alternate")
		alternate = true;
	LSARG_CASE('i', "ignore-case")
		ignore_case = true;
	WRONG_ARG
		cout << argv0 << ": Bad argument: " << argv[0] << '\n';
		return EXIT_FAILURE;
	ARGEND;

	if(argc != 0) {
		cout << "Extra arguments\n";
		return EXIT_FAILURE;
	}

	bool use_until = until.size();
	bool use_after = after.size();

	if(alternate && !(use_until && use_after)) {
		cout << "--alternate requires both --after and --until\n";
		return EXIT_FAILURE;
	}

	RE2::Options options{RE2::Quiet};

	if(ignore_case)
		options.set_case_sensitive(false);

	RE2 rafter{after, options};
	RE2 runtil{until, options};

	if(!rafter.ok()) {
	cout << "Bad regex: " << after << '\n';
		cout << rafter.error() << '\n';
		return EXIT_FAILURE;
	}
	if(!runtil.ok()) {
		cout << "Bad regex: " << until << '\n';
		cout << runtil.error() << '\n';
		return EXIT_FAILURE;
	}

	FILE* in = fopen("/dev/stdin", "r");

	if(in == nullptr) {
		return EXIT_FAILURE;
	}

	size_t linesize = 0;
	ssize_t length = 0;
	char* line = nullptr;

start_over_again:
	if(use_after) {
		while((length = getline(&line, &linesize, in)) > 0) {
			line[length - 1] = '\0';
			if(RE2::PartialMatch(line, rafter)) {
				break;
			}
		}
	}

	if(use_until) {
		while((length = getline(&line, &linesize, in)) > 0) {
			line[length - 1] = '\0';

			if(RE2::PartialMatch(line, runtil)) {
				if(alternate)
					goto start_over_again;
				else
					break;
			}
			line[length - 1] = '\n';

			cout << line;
		}
	} else {
		while(getline(&line, &linesize, in) > 0) {
			cout << line;
		}
	}

	free(line);
	fclose(in);

	return EXIT_SUCCESS;
}
