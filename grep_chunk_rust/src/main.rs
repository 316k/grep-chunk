use std::env;
use std::process;
use regex::Regex;
use std::io;
use std::io::Write;
use std::io::BufRead;

fn main() -> Result<(), std::io::Error> {
    let mut next_is_value = false;
    let mut next_container = "";
    let mut until = "".to_string();
    let mut after = "".to_string();
    let mut ignore_case = false;
    let mut alternate = false;

    for argument in env::args() {
        if next_is_value {
            if next_container == "until" {
                until = argument.clone();
            }
            if next_container == "after" {
                after = argument.clone();
            }
            next_is_value = false;
            continue;
        } else if argument == "--until" || argument == "-u" {
            next_is_value = true;
            next_container = "until";
        } else if argument == "--after" || argument == "-a" {
            next_is_value = true;
            next_container = "after";
        } else if argument == "--alternate" || argument == "-x" {
            alternate = true;
        } else if argument == "--ignore-case" || argument == "-i" {
            ignore_case = true;
        }
    }

    let use_after = after != "";
    let use_until = until != "";
    if alternate && !use_until && !use_after {
        println!( "--alternate requires both --after and --until");
        process::exit(1);
    }

    let mut after_regex = Regex::new("").unwrap();
    let mut until_regex = Regex::new("").unwrap();

    if use_after {
        let after_regex_result = Regex::new(&after);
        after_regex = match after_regex_result {
            Ok(regex) => regex,
            Err(error) => panic!("Bad regex: {:?}", error),
        };
    }
    if use_until {
        let until_regex_result = Regex::new(&until);
        until_regex = match until_regex_result {
            Ok(regex) => regex,
            Err(error) => panic!("Bad regex: {:?}", error),
        };
    }
    let mut lllline;
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();
    loop {
        if use_after {
            loop {
                lllline = lines.next().unwrap().unwrap();
                if lllline == "" {
                    process::exit(0);
                }
                // println!("aaaa{}", lllline);
                if after_regex.is_match(&lllline) {
                    break;
                }
            }
        }
        if use_until {
            loop {
                lllline = lines.next().unwrap().unwrap();
                if lllline == "" {
                    process::exit(0);
                }
                // println!("bbbb{}", lllline);
                if until_regex.is_match(&lllline) {
                    if alternate {
                        break;
                    } else {
                        process::exit(0);
                    }
                }
                io::stdout().write(lllline.as_bytes());
                io::stdout().write("\n".as_bytes());

            }
        } else {
            loop {
                lllline = lines.next().unwrap().unwrap();
                if lllline == "" {
                    process::exit(0);
                }
                // println!("cccc{}", lllline == "");
                io::stdout().write(lllline.as_bytes());
                io::stdout().write("\n".as_bytes());
            }
        }
    }
}
