#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <string.h>
#include "args.h"

int main(int argc, const char** argv) {

	int alternate = 0, ignore_case = 0;
	const char *until = "", *after = "";
	int use_until = 0, use_after = 0;

	ARGBEGIN
	LSARG_CASE('a', "after")
		after = ARGS;
		use_after = 1;
	LSARG_CASE('u', "until")
		until = ARGS;
		use_until = 1;
	LSARG_CASE('x', "alternate")
		alternate = 1;
	LSARG_CASE('i', "ignore-case")
		ignore_case = 1;
	WRONG_ARG
		printf("%s: Bad argument: %s\n", argv0, argv[0]);
		return EXIT_FAILURE;
	ARGEND;

	if(argc != 0) {
		printf("Extra arguments\n");
		return EXIT_FAILURE;
	}


	if(alternate && !(use_until && use_after)) {
		printf("--alternate requires both --after and --until\n");
		return EXIT_FAILURE;
	}

	int options = REG_EXTENDED | REG_NOSUB;

	if(ignore_case)
		options = options | REG_ICASE;

	regex_t rafter;
	if(use_after) {
		if(regcomp(&rafter, after, options) != 0) {
			printf("Bad regex: %s\n", after);
		}
	}

	regex_t runtil;
	if(use_until) {
		if(regcomp(&runtil, until, options) != 0) {
			printf("Bad regex: %s\n", until);
		}
	}

	size_t linesize = 0;
	ssize_t length = 0;
	char* line = NULL;

start_over_again:
	if(use_after) {
		while((length = getline(&line, &linesize, stdin)) > 0) {
			line[length - 1] = '\0';
			if(regexec(&rafter, line, 0, NULL, 0) != REG_NOMATCH)
				break;
		}
	}


	if(use_until) {
		while((length = getline(&line, &linesize, stdin)) > 0) {
			line[length - 1] = '\0';

			if(regexec(&runtil, line, 0, NULL, 0) != REG_NOMATCH) {
				if(alternate)
					goto start_over_again;
				else
					break;
			}
			line[length - 1] = '\n';

			printf("%s", line);
		}
	} else {
		while((length = getline(&line, &linesize, stdin)) > 0) {
			printf("%s", line);
		}
	}

	if(use_until)
		regfree(&runtil);
	if(use_after)
		regfree(&rafter);
        
	return EXIT_SUCCESS;
}
