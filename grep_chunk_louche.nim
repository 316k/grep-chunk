import std/re
import std/parseopt
import system

var opt_parser = initOptParser()

var filename: string
var after = ""
var until = ""
var ignore_case: bool
var alternate: bool

for kind, key, val in opt_parser.getopt():
  case kind
  of cmdArgument:
    filename = key
  of cmdLongOption, cmdShortOption:
    case key
    of "after", "a":
      after = val
    of "until", "u":
      until = val
    of "ignore-case", "i":
      ignore_case = true
    of "alternate", "x":
      alternate = true
  of cmdEnd:
    discard

var use_after = after.len > 0
var use_until = until.len > 0

if alternate and not use_after and not use_until:
  echo "--alternate requires both --after and --until"
  quit(1)

var after_regex: Regex
var until_regex: Regex
try:
  if use_after:
    after_regex = re(after)
  if use_until:
    until_regex = re(until)
except RegexError as ex:
  echo "Bad regex"
  echo ex.msg
  quit(1)


var line: string
try:
  # try catch to avoid writing the exception to stdout when the eof happens
  while true:
    if use_after:
      while true:
        if contains(readLine(stdin), after_regex):
          break

    if use_until:
      while true:
        line = readLine(stdin)
        if contains(line, until_regex):
          if alternate:
            break
          else:
            quit()
        writeLine(stdout, line)
    else:
      while true:
        writeLine(stdout, readLine(stdin))
except:
  quit(0)
