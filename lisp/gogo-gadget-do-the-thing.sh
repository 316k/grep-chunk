#!/usr/bin/env bash

# curl -O https://beta.quicklisp.org/quicklisp.lisp
# sbcl --eval '(load "quicklisp.lisp")' \
#      --eval '(quicklisp-quickstart:install)' \
#      --eval '(ql:add-to-init-file)'

sbcl --load grep-chunk.lisp    

# seq 10000000 40000000 | ./grep-chunk 20 30
