(ql:quickload :cl-argparse)
(ql:quickload :cl-ppcre)

(defun read-my-line ()
  #+:win32
  (read-line *terminal-io* nil)
  #+:linux
  (read-line))

(defun parse-arg ()
  (cl-argparse:parse
   (cl-argparse:create-main-parser (main-parser "here comes the description of the program")
     (cl-argparse:add-optional main-parser :short "s" :long "start" :help "th"
                                           :var "start")
     (cl-argparse:add-optional main-parser :short "e" :long "end" :help "th"
                                           :var "end"))
   (uiop:command-line-arguments)))

(defun calice ()
  (let* ((args (parse-arg))
         start startp
         end endp
         (col nil))
    (multiple-value-bind (val valp)
        (cl-argparse:get-value "start" args)
      (setf start (if valp val "")
            startp valp))
    (multiple-value-bind (val valp)
        (cl-argparse:get-value "end" args)
      (setf end (if valp val "")
            endp valp))
    (let ((start-scanner (cl-ppcre:create-scanner start))
          (end-scanner (cl-ppcre:create-scanner end)))
      (loop for line = (read-my-line)
            while line do
              (progn
                (when (and col endp)
                  (if (cl-ppcre:scan end-scanner line)
                      (progn
                        (return-from calice))))
                (when col
                  (write-string line)
                  (write-char #\newline))
                (when (and (null col) startp)
                  (if (cl-ppcre:scan start-scanner line)
                      (setf col t))))))))

(sb-ext:save-lisp-and-die "grep-chunk" :toplevel #'calice :executable t)
