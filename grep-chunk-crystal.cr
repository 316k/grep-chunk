require "option_parser"
require "regex"

until_string = ""
after_string = ""
alternate = false

OptionParser.parse do |parser|
  parser.on("-u UNTIL", "--until=UNTIL", "until regex") { |until_| until_string = until_ }
  parser.on("-a AFTER", "--after=AFTER", "after regex") { |after_| after_string = after_ }
  parser.on("-x", "--alternate", "goes back to until when finished after") { alternate = true }
  parser.invalid_option do |flag|
    STDERR.puts "ERROR: #{flag} is not a valid option."
    STDERR.puts parser
    exit(1)
  end
end

use_until = until_string != ""
use_after = after_string != ""

if alternate && (!use_after && !use_until)
  puts "Can't use --alternate without both --until and --after"
  exit(1)
end

after_regex = Regex.new("")
until_regex = Regex.new("")

begin
  if use_after
    after_regex = Regex.new(after_string)
  end
  if use_until
    until_regex = Regex.new(until_string)
  end
rescue ex
  puts "Bad regex #{ex.message}"
end

line = ""

  # try catch to avoid writing the exception to stdout when the eof happens
begin
  while true
    if use_after
      while true
        if after_regex.matches?(STDIN.read_line())
          break
        end
      end
    end
    if use_until
      while true
        line = STDIN.read_line()
        if until_regex.matches?(line)
          if alternate
            break
          else
            exit(0)
          end
        end
        puts(line)
      end
    else
      while true
        puts(STDIN.read_line())
      end
    end
  end
rescue
  exit(0)
end
