#!/usr/bin/env pypy3

import argparse
import sys
import re
parser = argparse.ArgumentParser()
parser.add_argument('-a', '--after')      # option that takes a value
parser.add_argument('-u', '--until')
parser.add_argument('-i', '--ignore-case')
parser.add_argument('-x', '--alternate', action="store_true")
args = parser.parse_args()

if args.alternate and not args.after and not args.until:
    print("--alternate requires both --after and --until")
    exit()

after_regex = None
until_regex = None

try:
    if args.after:
        after_regex = re.compile(args.after)
    if args.until:
        until_regex = re.compile(args.until)
except Exception as e:
    print("Bad regex")
    print(e)


line = None
while True:
    if args.after:
        while True:
            line = sys.stdin.readline()
            if not line:
                exit()
            if re.search(after_regex, line):
                break

    if args.until:
        while True:
            line = sys.stdin.readline()
            if not line:
                exit()
            if re.search(until_regex, line):
                if args.alternate:
                    break
                else:
                    exit()
            sys.stdout.write(line)
    else:
        while True:
            line = sys.stdin.readline()
            if not line:
                exit()
            sys.stdout.write(line)
