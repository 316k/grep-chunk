#!/bin/bash

export TIMEFORMAT="%R" LC_ALL=C

(
# Very simple check
seq 1 10 | ./grep-until 5 | md5sum
seq 1 10 | ./grep-chunks --until 5 | md5sum
seq 1 10 | ./grep-chunks-dumbcpp --until 5 | md5sum
seq 1 10 | ./grep-chunks-gnuc --until 5 | md5sum
seq 1 10 | ./grep_chunk_louche --until=5 | md5sum
seq 1 10 | ./grep_chunk_louche.py --until=5 | md5sum
seq 1 10 | ./grep-chunk-crystal --until=5 | md5sum
seq 1 10 | sed -r '/5/Q' | md5sum

seq 1 10 | ./grep-after 5 | md5sum
seq 1 10 | ./grep-chunks --after 5 | md5sum
seq 1 10 | ./grep-chunks-dumbcpp --after 5 | md5sum
seq 1 10 | ./grep-chunks-gnuc --after 5 | md5sum
seq 1 10 | ./grep_chunk_louche --after=5 | md5sum
seq 1 10 | ./grep_chunk_louche.py --after=5 | md5sum
seq 1 10 | ./grep-chunk-crystal --after=5 | md5sum
seq 1 10 | sed -nr '/5/,$p' | sed 1d | md5sum

# (all)
seq 1 10 | ./grep-until foo | md5sum
seq 1 10 | ./grep-chunks --until foo | md5sum
seq 1 10 | ./grep-chunks-dumbcpp --until foo | md5sum
seq 1 10 | ./grep-chunks-gnuc --until foo | md5sum
seq 1 10 | ./grep_chunk_louche --until=foo | md5sum
seq 1 10 | ./grep_chunk_louche.py --until=foo | md5sum
seq 1 10 | ./grep-chunk-crystal --until=foo | md5sum
seq 1 10 | sed -r '/foo/Q' | md5sum

# (nothing)
seq 1 10 | ./grep-after foo | md5sum
seq 1 10 | ./grep-chunks --after foo | md5sum
seq 1 10 | ./grep-chunks-dumbcpp --after foo | md5sum
seq 1 10 | ./grep-chunks-gnuc --after foo | md5sum
seq 1 10 | ./grep_chunk_louche --after=foo | md5sum
seq 1 10 | ./grep_chunk_louche.py --after=foo | md5sum
seq 1 10 | ./grep-chunk-crystal --after=foo | md5sum
seq 1 10 | sed -nr '/foo/,$p' | sed 1d | md5sum

# after + until
seq 1 10 | ./grep-after 3 | ./grep-until 7 | md5sum
seq 1 10 | ./grep-chunks -a 3 -u 7 | md5sum
seq 1 10 | ./grep-chunks-dumbcpp -a 3 -u 7 | md5sum
seq 1 10 | ./grep-chunks-gnuc -a 3 -u 7 | md5sum
seq 1 10 | ./grep_chunk_louche -a=3 -u=7 | md5sum
seq 1 10 | ./grep_chunk_louche.py -a=3 -u=7 | md5sum
seq 1 10 | ./grep-chunk-crystal -a 3 -u 7 | md5sum
seq 1 10 | sed -n '/3/,/7/p' | sed '1d;$d' | md5sum

) | uniq -c

# Pure text
echo until 39990000
echo -n "awk "; time seq 10000000 40000000 | ./grep-until 39990000 >/dev/null
echo -n "c++ "; time seq 10000000 40000000 | ./grep-chunks -u 39990000 >/dev/null
echo -n "c   "; time seq 10000000 40000000 | ./grep-chunks-gnuc -u 39990000 >/dev/null
echo -n "stl "; time seq 10000000 40000000 | ./grep-chunks-dumbcpp -u 39990000 >/dev/null
echo -n "nim "; time seq 10000000 40000000 | ./grep_chunk_louche -u=39990000 >/dev/null
echo -n "py  "; time seq 10000000 40000000 | ./grep_chunk_louche.py -u=39990000 >/dev/null
echo -n "cry "; time seq 10000000 40000000 | ./grep-chunk-crystal -u 39990000 >/dev/null
echo -n "sed "; time seq 10000000 40000000 |  sed -r '/39990000/Q' >/dev/null

echo after 39990000
echo -n "awk "; time seq 10000000 40000000 | ./grep-after 39990000 >/dev/null
echo -n "c++ "; time seq 10000000 40000000 | ./grep-chunks -a 39990000 >/dev/null
echo -n "c   "; time seq 10000000 40000000 | ./grep-chunks-gnuc -a 39990000 >/dev/null
echo -n "stl "; time seq 10000000 40000000 | ./grep-chunks-dumbcpp -a 39990000 >/dev/null
echo -n "nim "; time seq 10000000 40000000 | ./grep_chunk_louche -a=39990000 >/dev/null
echo -n "py  "; time seq 10000000 40000000 | ./grep_chunk_louche.py -a=39990000 >/dev/null
echo -n "cry "; time seq 10000000 40000000 | ./grep-chunk-crystal -a 39990000 >/dev/null
echo -n "sed "; time seq 10000000 40000000 |  sed -nr '/39990000/,$p' |  sed '1d' >/dev/null

# Simple
echo until "'[3-9]9990000'"
echo -n "awk "; time seq 10000000 40000000 | ./grep-until '[3-9]9990000' >/dev/null
echo -n "c++ "; time seq 10000000 40000000 | ./grep-chunks -u '[3-9]9990000' >/dev/null
echo -n "c   "; time seq 10000000 40000000 | ./grep-chunks-gnuc -u '[3-9]9990000' >/dev/null
echo -n "stl "; time seq 10000000 40000000 | ./grep-chunks-dumbcpp -u '[3-9]9990000' >/dev/null
echo -n "nim "; time seq 10000000 40000000 | ./grep_chunk_louche -u='[3-9]9990000' >/dev/null
echo -n "py  "; time seq 10000000 40000000 | ./grep_chunk_louche.py -u='[3-9]9990000' >/dev/null
echo -n "cry "; time seq 10000000 40000000 | ./grep-chunk-crystal -u '[3-9]9990000' >/dev/null
echo -n "sed "; time seq 10000000 40000000 |  sed -r '/[3-9]9990000/Q' >/dev/null

echo after "'[3-9]9990000'"
echo -n "awk "; time seq 10000000 40000000 | ./grep-after '[3-9]9990000' >/dev/null
echo -n "c++ "; time seq 10000000 40000000 | ./grep-chunks -a '[3-9]9990000' >/dev/null
echo -n "c   "; time seq 10000000 40000000 | ./grep-chunks-gnuc -a '[3-9]9990000' >/dev/null
echo -n "stl "; time seq 10000000 40000000 | ./grep-chunks-dumbcpp -a '[3-9]9990000' >/dev/null
echo -n "nim "; time seq 10000000 40000000 | ./grep_chunk_louche -a='[3-9]9990000' >/dev/null
echo -n "py  "; time seq 10000000 40000000 | ./grep_chunk_louche.py -a='[3-9]9990000' >/dev/null
echo -n "cry "; time seq 10000000 40000000 | ./grep-chunk-crystal -a '[3-9]9990000' >/dev/null
echo -n "sed "; time seq 10000000 40000000 |  sed -nr '/[3-9]9990000/,$p' |  sed '1d' >/dev/null

# Troll pattern (almost-but-not-found)
echo until "'[3-9][1-9][2-7][4-9]1[0-9][1-9]X'"
echo -n "awk "; time seq 10000000 40000000 | ./grep-until '[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "c++ "; time seq 10000000 40000000 | ./grep-chunks -u '[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "c   "; time seq 10000000 40000000 | ./grep-chunks-gnuc -u '[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "stl "; time seq 10000000 40000000 | ./grep-chunks-dumbcpp -u '[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "nim "; time seq 10000000 40000000 | ./grep_chunk_louche -u='[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "py  "; time seq 10000000 40000000 | ./grep_chunk_louche.py -u='[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "cry "; time seq 10000000 40000000 | ./grep-chunk-crystal -u '[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "sed "; time seq 10000000 40000000 |  sed -r '/[3-9][1-9][2-7][4-9]1[0-9][1-9]X/Q' >/dev/null

echo after "'[3-9][1-9][2-7][4-9]1[0-9][1-9]X'"
echo -n "awk "; time seq 10000000 40000000 | ./grep-after '[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "c++ "; time seq 10000000 40000000 | ./grep-chunks -a '[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "c   "; time seq 10000000 40000000 | ./grep-chunks-gnuc -a '[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "stl "; time seq 10000000 40000000 | ./grep-chunks-dumbcpp -a '[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "nim "; time seq 10000000 40000000 | ./grep_chunk_louche -a='[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "py  "; time seq 10000000 40000000 | ./grep_chunk_louche.py -a='[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "cry "; time seq 10000000 40000000 | ./grep-chunk-crystal -a '[3-9][1-9][2-7][4-9]1[0-9][1-9]X' >/dev/null
echo -n "sed "; time seq 10000000 40000000 |  sed -nr '/[3-9][1-9][2-7][4-9]1[0-9][1-9]X/,$p' |  sed '1d' >/dev/null
