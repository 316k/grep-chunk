#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <regex>
#include "args.h"

using namespace std;

int main(int argc, const char** argv) {

	bool alternate = false, ignore_case = false;
	string until{""}, after{""};

	ARGBEGIN
		LSARG_CASE('a', "after")
		after = ARGS;
	LSARG_CASE('u', "until")
		until = ARGS;
	LSARG_CASE('x', "alternate")
		alternate = true;
	LSARG_CASE('i', "ignore-case")
		ignore_case = true;
	WRONG_ARG
		cout << argv0 << ": Bad argument: " << argv[0] << '\n';
		return EXIT_FAILURE;
	ARGEND;

	if(argc != 0) {
		cout << "Extra arguments\n";
		return EXIT_FAILURE;
	}

	bool use_until = until.size();
	bool use_after = after.size();

	if(alternate && !(use_until && use_after)) {
		cout << "--alternate requires both --after and --until\n";
		return EXIT_FAILURE;
	}

	auto options = regex::egrep;

	if(ignore_case)
		options = options | regex::icase;

	regex rafter{after, options};
	regex runtil{until, options};

	ifstream in{"/dev/stdin"};

	string line;

start_over_again:
	if(use_after) {
		while(getline(in, line)) {
			// line[line.size() - 1] = '\0';
			if(regex_search(line, rafter)) {
				break;
			}
		}
	}

	if(use_until) {
		while(getline(in, line)) {
			// int n = line.size() - 1;
			// line[n] = '\0';

			if(regex_search(line, runtil)) {
				if(alternate)
					goto start_over_again;
				else
					break;
			}
			// line[n] = '\n';

			cout << line << '\n';
		}
	} else {
		while(getline(in, line)) {
			cout << line << '\n';
		}
	}

	return EXIT_SUCCESS;
}
