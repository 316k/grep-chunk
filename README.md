grep-chunks
===========

Print all lines after and/or until regexes are matched

```bash
$ seq 1 10 | grep-chunks --after 5
6
7
8
9
10
$ seq 1 10 | grep-chunks --until 5
1
2
3
4
$ seq 1 20 | ./grep-chunks -a '4$' -u '8$'
5
6
7
$ seq 1 20 | ./grep-chunks -a '4$' -u '8$' --alternate
5
6
7
15
16
17
```

## Build

```bash
$ apt install libre2-dev
$ make
```
